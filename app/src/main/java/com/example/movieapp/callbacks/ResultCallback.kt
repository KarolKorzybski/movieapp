package com.example.movieapp.callbacks

import com.example.movieapp.models.Result

interface ResultCallback {
    fun getResult(result : Result)
    fun clickFavoriteButton(result : Result, position: Int)
}