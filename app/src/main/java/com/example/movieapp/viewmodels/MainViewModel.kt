package com.example.movieapp.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movieapp.api.response.GetMovieDetailsResponse
import com.example.movieapp.api.response.GetMoviesResultResponse
import com.example.movieapp.enums.SortingEnum
import com.example.movieapp.models.ListResultSetting
import com.example.movieapp.models.Result
import com.example.movieapp.repository.DetailsMovieRepository
import com.example.movieapp.repository.MovieNowPlayingRepository
import com.example.movieapp.repository.ResultDaoRepository
import com.example.movieapp.repository.SearchMoviesRepository
import com.example.movieapp.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private var resultDaoRepository: ResultDaoRepository,
    private var nowPlayingRepository: MovieNowPlayingRepository,
    private var searchMoviesRepository: SearchMoviesRepository,
    private var detailsMovieRepository: DetailsMovieRepository
) : ViewModel() {

    var findText = ""
    lateinit var settingNowPlaying : ListResultSetting
    lateinit var settingFindMovie : ListResultSetting
    var stateButtonBack = false
    var positionViewPager = 0
    var resultDao = ArrayList<Result>()
    var progressBarLD = SingleLiveEvent<Boolean?>()
    var gotoMovieDetails = SingleLiveEvent<Result>()
    var nowPlayingLD = SingleLiveEvent<GetMoviesResultResponse?>()
    var searchMoviesLD = SingleLiveEvent<GetMoviesResultResponse?>()
    var detailsMovieLD = SingleLiveEvent<GetMovieDetailsResponse?>()
    var sortingFieldLD = MutableLiveData<SortingEnum?>()

    fun getNowPlaying(page: Double = 1.0) {
        nowPlayingRepository.getNowPlaying(
            page,
            {
                settingNowPlaying = ListResultSetting(
                    lastPage = page.toDouble(),
                    lastItem = nowPlayingLD.value?.results?.lastIndex?.toDouble()?:0.0,
                    totalPage = it?.totalPages?:0.0
                )
                val response = filterWithFavoriteListAndAddNewItems(it, nowPlayingLD.value?.results)
                nowPlayingLD.postValue(response)
            }, {
                nowPlayingLD.postValue(null)
            }, {

            })
    }

    fun getSearchMovies(query: String, page : Double = 1.0) {
        searchMoviesRepository.getSearchMovies(
            query,
            page,
            {
            settingFindMovie = ListResultSetting(
                lastPage = page,
                lastItem = searchMoviesLD.value?.results?.lastIndex?.toDouble()?:0.0,
                totalPage = it?.totalPages?:0.0
            )
            val response = filterWithFavoriteListAndAddNewItems(it, searchMoviesLD.value?.results)
            searchMoviesLD.postValue(response)
        }, {
            searchMoviesLD.postValue(null)
        }, {

        })
    }

    fun getDetailsMovie(id: String) {
        detailsMovieRepository.getMovieDetails(
            id, {
                detailsMovieLD.postValue(it)
            }, {
                detailsMovieLD.postValue(null)
            }, {

            })
    }

    private fun filterWithFavoriteListAndAddNewItems(response: GetMoviesResultResponse?, resultsAdding : List<Result>?): GetMoviesResultResponse? {
        var list = ArrayList<Result>()
        list.addAll(resultsAdding?: arrayListOf())
        list.addAll(response?.results?: arrayListOf())
        list = ArrayList(list.distinctBy { it.id })
        response?.results = list
        response?.results?.let {
            it.forEachIndexed { _, result ->
                result.isFavorite =
                    !resultDao.filter { itemDao -> itemDao.id == result.id }.isNullOrEmpty()
            }
        }
        return response
    }

    fun getResultDao() = resultDaoRepository.getAll()

    fun addResult(result: Result, isDone: (() -> Unit)? = null) {
        resultDaoRepository.addResult(result, resultDao) {
            isDone?.invoke()
        }
    }

    fun removeResult(result: Result, isDone: (() -> Unit)? = null) {
        resultDaoRepository.removeResult(result, resultDao) {
            isDone?.invoke()
        }
    }

    fun loadingNewItems() {
        progressBarLD.value = true
        getNowPlaying(settingNowPlaying.lastPage.plus(1.0))
    }

    fun loadingNewFindItems() {
        progressBarLD.value = true
        getSearchMovies(findText, settingFindMovie.lastPage.plus(1.0))
    }

    fun canLoadingNowPlayingPage() = settingNowPlaying.totalPage > settingNowPlaying.lastPage
    fun canLoadingFindPage() = settingFindMovie.totalPage > settingFindMovie.lastPage
}