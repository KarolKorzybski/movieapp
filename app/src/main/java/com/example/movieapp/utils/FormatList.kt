package com.example.movieapp.utils

import com.example.movieapp.enums.SortingEnum
import com.example.movieapp.models.Result

object FormatList {

    fun filterListByTime(movies: List<Result>?, enum: SortingEnum? = null): List<Result> {
        return when(enum){
            SortingEnum.LIKE -> movies?.sortedByDescending { it.voteCount?:0 }?: arrayListOf()
            SortingEnum.POPULARITY -> movies?.sortedByDescending { it.popularity?:0.0 }?: arrayListOf()
            else->movies?.sortedByDescending { FormatTime.changeFormatDateToTime(it.releaseDate ?: "") }?: arrayListOf()
        }
    }
}