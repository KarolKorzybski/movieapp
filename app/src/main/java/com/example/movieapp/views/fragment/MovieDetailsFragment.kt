package com.example.movieapp.views.fragment

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.FitCenter
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.request.transition.TransitionFactory
import com.example.movieapp.BuildConfig
import com.example.movieapp.R
import com.example.movieapp.api.response.GetMovieDetailsResponse
import com.example.movieapp.databinding.MovieDetailsBinding
import com.example.movieapp.models.ProductionCompany
import com.example.movieapp.models.Result
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.views.activity.MainActivity
import com.mikhaellopez.rxanimation.*
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MovieDetailsFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var binding: MovieDetailsBinding
    private lateinit var clickedResult: Result

    private fun init() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    private fun observable() {
        viewModel.detailsMovieLD.observe(viewLifecycleOwner, {
            setListGenres(it)
            setHomePage(it?.homepage)
            setBudget(it?.budget)
        })
    }

    private fun setBudget(budget: Int?) {
        if(budget == null || budget == 0){
            binding.budgetTitle.visibility = View.GONE
            binding.budget.visibility = View.GONE
        }else{
            binding.budgetTitle.visibility = View.VISIBLE
            binding.budget.visibility = View.VISIBLE
            binding.budget.text = "$budget$"
        }
    }

    private fun setHomePage(homepage: String?) {
        binding.homePage.text = homepage ?: ""
        binding.homePage.visibility = if (homepage.isNullOrEmpty()) View.GONE else View.VISIBLE
    }

    private fun setListGenres(response: GetMovieDetailsResponse?) {
        if (!response?.genres.isNullOrEmpty()) {
            val text =
                response?.genres?.map { it.name }.toString().replace("[", "").replace("]", "")
            binding.genres.text = text
            binding.genres.visibility = View.VISIBLE
            binding.typeGenres.visibility = View.VISIBLE
        } else {
            binding.genres.visibility = View.GONE
            binding.typeGenres.visibility = View.GONE
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.movie_details_fragment, container, false)
        init()
        observable()
        getDetailsMovie()
        initFields()
        setListeners()
        return binding.root
    }

    private fun getDetailsMovie() {
        viewModel.getDetailsMovie(clickedResult.id.toString())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideAnimation()
    }

    private fun hideAnimation() {
        (activity as? MainActivity)?.initAnimation(false) {
        }
    }

    private fun initFields() {
        Glide
            .with(requireContext())
            .load(
                BuildConfig.IMAGE_ORIGINAL + (clickedResult.posterPath
                    ?: clickedResult.backdropPath)
            )
            .transition(GenericTransitionOptions.with(R.anim.fade))
            .fitCenter()
            .apply(RequestOptions().fitCenter())
            .into(binding.logo)

        binding.title.text = clickedResult.title ?: ""
        binding.description.text = clickedResult.overview ?: ""
        binding.date.text = clickedResult.releaseDate ?: ""
        binding.popularity.text = clickedResult.popularity.toString()
        binding.language.text = clickedResult.originalLanguage ?: ""
        binding.vote.text = clickedResult.voteCount.toString()


    }

    private fun setListeners() {
    }

    companion object {
        @JvmStatic
        fun newInstance(result: Result): MovieDetailsFragment {
            val fragment = MovieDetailsFragment()
            fragment.clickedResult = result
            return fragment
        }
    }
}