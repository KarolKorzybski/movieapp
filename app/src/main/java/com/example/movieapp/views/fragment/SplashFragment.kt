package com.example.movieapp.views.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.movieapp.R
import com.example.movieapp.databinding.SplashFragmentBinding
import com.example.movieapp.utils.SingleLiveEvent
import com.example.movieapp.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashFragment : Fragment() {

    var time : Long = 0
    var isReadyLD = SingleLiveEvent<Boolean>()
    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var binding: SplashFragmentBinding

    private fun init() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }
    private fun observable(){
        viewModel.nowPlayingLD.observe(viewLifecycleOwner,{
            val diff = System.currentTimeMillis().minus(time)
            if(diff < TIME_TO_RUN){
                gotoNextFragment(TIME_TO_RUN.minus(diff))
            }else{
                gotoNextFragment(0)
            }
        })
        isReadyLD.observe(viewLifecycleOwner,{
            val neFragment: Fragment = MainMoviesFragment.newInstance()
            parentFragmentManager.beginTransaction().replace(R.id.frameLayout, neFragment).commit()
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.splash_fragment,container,false)
        init()
        observable()
        getNowPlaying()
        return binding.root
    }

    private fun getNowPlaying() {
        time = System.currentTimeMillis()
        viewModel.getNowPlaying()
    }

    private fun gotoNextFragment(diff: Long) {
        Handler(Looper.getMainLooper()).postDelayed({
            isReadyLD.value = true
        },diff)
    }

    companion object {
        @JvmStatic
        fun newInstance() = SplashFragment()
        const val TIME_TO_RUN = 3000L //second
    }
}
