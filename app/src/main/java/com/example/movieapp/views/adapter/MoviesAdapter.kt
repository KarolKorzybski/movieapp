package com.example.movieapp.views.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.movieapp.BuildConfig
import com.example.movieapp.R
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.databinding.MovieRow
import com.example.movieapp.models.Result
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.alpha
import com.mikhaellopez.rxanimation.backgroundColor
import com.mikhaellopez.rxanimation.press

class MoviesAdapter(var list: List<Result>, private val callback: ResultCallback) :
    RecyclerView.Adapter<MoviesAdapter.RegistrationView>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RegistrationView {
        val adapterRow: MovieRow = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(viewGroup.context),
            R.layout.movie_row,
            viewGroup,
            false
        ) as MovieRow
        return RegistrationView(
            adapterRow,
            callback
        )
    }

    override fun onBindViewHolder(viewHolder: RegistrationView, position: Int) {
        val result = list[position]
        viewHolder.bind(result, position)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    class RegistrationView(private val adapterRow: MovieRow, private val callback: ResultCallback) :
        RecyclerView.ViewHolder(adapterRow.root) {
        var isBusy = false
        fun bind(result: Result, position: Int) {
            adapterRow.logo.requestLayout()
            result.posterPath?.let {
                Glide
                    .with(adapterRow.root.context)
                    .load(BuildConfig.IMAGE_500 + (it))
                    .transition(GenericTransitionOptions.with(R.anim.fade))
                    .fitCenter()
//                    .apply(RequestOptions().fitCenter())
//                    .dontAnimate()
                    .into(adapterRow.logo)
            }

            adapterRow.title.text = result.title ?: ""
            adapterRow.date.text = result.releaseDate ?: ""
            adapterRow.date.visibility =
                if (result.releaseDate.isNullOrEmpty()) View.GONE else View.VISIBLE
            adapterRow.dateTitle.visibility =
                if (result.releaseDate.isNullOrEmpty()) View.INVISIBLE else View.VISIBLE
            adapterRow.popularity.text = result.popularity.toString()
            adapterRow.language.text = result.originalLanguage ?: ""
            adapterRow.vote.text = result.voteCount.toString()
            adapterRow.favoriteButton.alpha = 1.0f
            adapterRow.unfavoriteButton.alpha = 1.0f
            adapterRow.favoriteButton.visibility = if (result.isFavorite) View.VISIBLE else View.GONE
            adapterRow.unfavoriteButton.visibility = if (!result.isFavorite) View.VISIBLE else View.GONE
            adapterRow.cardView.setOnClickListener {
                if (isBusy) return@setOnClickListener
                isBusy = true
                RxAnimation.from(it).press(duration = 500).subscribe {
                    callback.getResult(result)
                    isBusy = false
                }
            }
            val listener = object : View.OnClickListener{
                override fun onClick(p0: View?) {
                    if (isBusy) return
                    isBusy = true
                    result.isFavorite = !result.isFavorite
                    if (result.isFavorite) {
                        adapterRow.unfavoriteButton.alpha(alpha = 0.0f, duration = 250).subscribe {
                            adapterRow.unfavoriteButton.visibility = View.GONE
                            adapterRow.favoriteButton.visibility = View.VISIBLE
                            adapterRow.favoriteButton.alpha = 0.0f
                            adapterRow.favoriteButton.alpha(alpha = 1.0f, duration = 250).subscribe {
                                callback.clickFavoriteButton(result, position)
                                isBusy = false
                            }
                        }
                    } else {

                        adapterRow.favoriteButton.alpha(alpha = 0.0f, duration = 250).subscribe {
                            adapterRow.favoriteButton.visibility = View.GONE
                            adapterRow.unfavoriteButton.visibility = View.VISIBLE
                            adapterRow.unfavoriteButton.alpha = 0.0f
                            adapterRow.unfavoriteButton.alpha(alpha = 1.0f, duration = 250).subscribe {
                                callback.clickFavoriteButton(result, position)
                                isBusy = false
                            }
                        }
                    }
                }

            }
            adapterRow.favoriteButton.setOnClickListener(listener)
            adapterRow.unfavoriteButton.setOnClickListener(listener)
        }
    }
}