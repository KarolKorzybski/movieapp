package com.example.movieapp.views.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.example.movieapp.R
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.databinding.MainViewPagerBinding
import com.example.movieapp.layouts.FadeOutTransformation
import com.example.movieapp.models.Result
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.viewpagers.MainViewPagerAdapter
import com.example.movieapp.views.activity.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainMoviesFragment : Fragment(), ResultCallback {

    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var binding: MainViewPagerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {

    }

    private fun setupViewPager() {
        if (binding.viewPager.adapter == null) binding.viewPager.adapter =
            MainViewPagerAdapter(childFragmentManager, this)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                viewModel.positionViewPager = position
                when(position){
                    0 -> binding.active.isChecked = true
                    1 -> binding.deactivate.isChecked = true
                    else -> binding.favorite.isChecked = true
                }
            }
        })
//        binding.viewPager.setPageTransformer(true, FadeOutTransformation())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.main_view_pager, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager()
        getObservable()
        setListeners()
    }

    private fun setListeners() {
        binding.active.setOnClickListener { v: View? ->
            binding.active.isChecked = true
            binding.viewPager.currentItem = 0
        }
        binding.deactivate.setOnClickListener { v: View? ->
            binding.deactivate.isChecked = true
            binding.viewPager.currentItem = 1
        }
        binding.favorite.setOnClickListener { v: View? ->
            binding.favorite.isChecked = true
            binding.viewPager.currentItem = 2
        }

    }

    private fun gotoMovieDetailsFragment(result: Result) {
        val fragment = MovieDetailsFragment.newInstance(result)
        parentFragmentManager.beginTransaction().replace(R.id.frameLayout, fragment)
            .addToBackStack("frags").commit()
    }


    private fun getObservable() {
        viewModel.gotoMovieDetails.observe(viewLifecycleOwner,{
            gotoMovieDetailsFragment(it)
        })
    }


    companion object {
        @JvmStatic
        fun newInstance(): MainMoviesFragment {
            val fragment = MainMoviesFragment()
            val b = Bundle()
            fragment.arguments = b
            return fragment
        }
    }

    override fun getResult(result: Result) {
        animationChange(result)
    }

    override fun clickFavoriteButton(result: Result, position: Int) {
        if(!result.isFavorite) viewModel.removeResult(result)
        else viewModel.addResult(result)

    }

    private fun animationChange(result: Result){
        (activity as? MainActivity)?.initAnimation(true) {
            viewModel.gotoMovieDetails.value = result
        }
    }
}