package com.example.movieapp.views.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.movieapp.R
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.databinding.FindMoviesFragmentBinding
import com.example.movieapp.models.ListResultSetting
import com.example.movieapp.models.Result
import com.example.movieapp.utils.FormatList
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.views.adapter.MoviesAdapter
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.press
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class FindMoviesFragment(val callback : ResultCallback, private val numberViewPager : Int) : Fragment() {

    private val viewModel: MainViewModel by activityViewModels()

    private lateinit var binding: FindMoviesFragmentBinding
    private lateinit var adapter: MoviesAdapter
    private var directionDown = false
    private var blockLoading = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    private fun initViewModel() {
    }

    fun hideSoftKeyboard() {
        val imm: InputMethodManager? =
            activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(view?.windowToken, 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.find_movies_fragment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDefaultFindText()
        resetSetting()
        scrollListener()
        setInitAdapter()
        getObservable()
        initEditText()
        setListeners()
    }

    private fun scrollListener(){
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                directionDown = dy > 0
            }
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (recyclerView.canScrollVertically(1).not()
                    && newState == RecyclerView.SCROLL_STATE_IDLE
                    && directionDown && !blockLoading && viewModel.canLoadingFindPage()) {
                    blockLoading = true
                    loadingNewItems()
                }
            }
        })
    }

    private fun loadingNewItems() {
        viewModel.loadingNewFindItems()
    }

    private fun setInitAdapter() {
        setAdapter(FormatList.filterListByTime(viewModel.searchMoviesLD.value?.results, viewModel.sortingFieldLD.value))
    }

    private fun setDefaultFindText() {
        binding.findText.setText(viewModel.findText)
    }

    private fun setListeners() {
        binding.confirm.setOnClickListener {
            if (!isShowProgress()) {
                resetSetting()
                removeList()
                val text = binding.findText.text.toString()
                viewModel.findText = text
                viewModel.getSearchMovies(text)
                setVisibleProgress(true)
                setHintButton()
                RxAnimation.from(it).press(duration = 300).subscribe()
            }
        }
        binding.filterOption.setOnClickListener {
            activity?.supportFragmentManager?.let {
                FilterOptionDialogFragment.newInstance().show(it,"")
            }
        }
        binding.exit.setOnClickListener {
            binding.findText.setText("")
        }
    }

    private fun removeList() {
        viewModel.searchMoviesLD.value?.results = arrayListOf()
    }

    private fun isShowProgress() = viewModel.progressBarLD.value == true

    private fun setVisibleProgress(visible : Boolean){
        viewModel.progressBarLD.value = visible
    }

    private fun resetSetting() {
        viewModel.settingFindMovie = ListResultSetting(
            lastPage = 1.0,
            lastItem = 0.0,
            totalPage = 0.0
        )
    }

    private fun getObservable() {
        viewModel.searchMoviesLD.observe(viewLifecycleOwner, {
            setVisibleProgress(false)
            blockLoading = false
            it?.let { response->
                hideSoftKeyboard()
                if (viewModel.settingFindMovie.lastPage!=1.0){
                    updateAdapter(response.results?: arrayListOf())
                }else{
                    setAdapter(FormatList.filterListByTime(response.results))
                }
            }
        })
        viewModel.sortingFieldLD.observe(viewLifecycleOwner,{
            if(viewModel.positionViewPager == numberViewPager) {
                setAdapter(FormatList.filterListByTime(viewModel.nowPlayingLD.value?.results, it))
            }
        })
    }

    private fun updateAdapter(result: List<Result>) {
        val positionStart = if(result.size > viewModel.settingFindMovie.lastItem) viewModel.settingFindMovie.lastItem else 0.0
        adapter.list = result
        adapter.notifyItemRangeChanged(positionStart.toInt(), adapter.itemCount)
    }

    private fun setAdapter(result: List<Result>) {
        adapter = MoviesAdapter(result, callback)
        binding.recyclerView.layoutManager = GridLayoutManager(context,2, GridLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
        setVisibleFilterOption(!result.isNullOrEmpty())
    }

    private fun setVisibleFilterOption(visible: Boolean) {
        binding.filterOption.visibility = if(visible) View.VISIBLE else View.GONE
    }

    private fun initEditText() {
        binding.findText.addTextChangedListener {
            setHintButton()
            setExitButton(binding.findText.text.toString().isEmpty())
        }
    }

    private fun setExitButton(hide: Boolean) {
        binding.exit.visibility = if (hide) View.GONE else View.VISIBLE
    }

    private fun setHintButton() {
        binding.confirm.visibility = if (differentText() && binding.findText.text.toString().isNotEmpty()) View.VISIBLE else View.GONE
    }

    private fun differentText() = viewModel.findText != binding.findText.text.toString()

    companion object {
        @JvmStatic
        fun newInstance(callback : ResultCallback, position : Int): FindMoviesFragment {
            val fragment = FindMoviesFragment(callback, position)
            val b = Bundle()
            fragment.arguments = b
            return fragment
        }
    }

}