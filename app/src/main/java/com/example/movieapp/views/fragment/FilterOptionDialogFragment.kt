package com.example.movieapp.views.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Point
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.movieapp.R
import com.example.movieapp.databinding.FilterOptionDialogFragmentBinding
import com.example.movieapp.enums.SortingEnum
import com.example.movieapp.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FilterOptionDialogFragment : DialogFragment() {

    private val viewModel: MainViewModel by activityViewModels()

    companion object {

        @SuppressLint("StaticFieldLeak")
        private lateinit var binding: FilterOptionDialogFragmentBinding

        @JvmStatic
        fun newInstance() = FilterOptionDialogFragment()
    }

    private fun init() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setInitCheckFields()
        setListeners()
        initBackground()
    }

    private fun setInitCheckFields() {
        when(viewModel.sortingFieldLD.value){
            SortingEnum.LIKE -> {
                uncheckedAllField()
                binding.rbByLike.isChecked = true
            }
            SortingEnum.POPULARITY -> {
                uncheckedAllField()
                binding.rbByPopularity.isChecked = true
            }
            else -> {
                uncheckedAllField()
                binding.rbByDate.isChecked = true
            }
        }
    }

    fun uncheckedAllField(){
        binding.rbByDate.isChecked = false
        binding.rbByLike.isChecked = false
        binding.rbByPopularity.isChecked = false
    }

    private fun initBackground() {
        dialog?.window?.setBackgroundDrawableResource(R.drawable.border_filter_option)
    }

    private fun setListeners() {
        binding.confirm.setOnClickListener {
            viewModel.sortingFieldLD.value = getSortingType()
            dismiss()
        }
    }

    private fun getSortingType(): SortingEnum? {
        return when {
            binding.rbByPopularity.isChecked -> SortingEnum.POPULARITY
            binding.rbByLike.isChecked -> SortingEnum.LIKE
            binding.rbByDate.isChecked -> SortingEnum.DATE
            else -> null
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.filter_option_dialog_fragment,
            container,
            false
        )
        return binding.root
    }


    override fun onResume() {
        super.onResume()

        dialog?.window?.apply {
            val size = Point()
            windowManager.defaultDisplay.getSize(size)

            setLayout(0.9 of size.x, ViewGroup.LayoutParams.WRAP_CONTENT)
//            setGravity(Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM)
            setGravity(Gravity.CENTER)
        }
    }

    private infix fun Double.of(value: Int) = (this * value).toInt()
}