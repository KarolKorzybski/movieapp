package com.example.movieapp.views.activity

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.example.movieapp.R
import com.example.movieapp.databinding.MainActivityBinding
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.views.fragment.SplashFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: MainActivityBinding

//    @Inject
//    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
//        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        init()
        startMainFragment()
        getObservable()
    }

    private fun getObservable() {
        viewModel.progressBarLD.observe(this, {
            setVisibleProgress(it)
        })
    }

    private fun init() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel.progressBarLD.call()
        supportActionBar?.hide()
    }

    private fun startMainFragment() {
        val newFragment: Fragment = SplashFragment.newInstance()
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout, newFragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun initAnimation(show: Boolean, callbackDoneAction: ((Boolean) -> Unit)) {
        binding.animationChange.visibility = View.VISIBLE
        binding.animationChange.initAnimation(show, callbackDoneAction)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && viewModel.stateButtonBack) {
            true
        } else super.onKeyDown(keyCode, event)
    }

    private fun setVisibleProgress(visible : Boolean?){
        binding.progressLayout.visibility = if(visible == true) View.VISIBLE else View.GONE
    }
}