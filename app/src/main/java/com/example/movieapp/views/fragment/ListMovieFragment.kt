package com.example.movieapp.views.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.databinding.ListMovieFragmentBinding
import com.example.movieapp.models.ListResultSetting
import com.example.movieapp.models.Result
import com.example.movieapp.utils.FormatList
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.views.adapter.MoviesAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class ListMovieFragment(val callback : ResultCallback, private val numberViewPager : Int) : Fragment() {

    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var binding: ListMovieFragmentBinding
    private lateinit var adapter: MoviesAdapter
    private var directionDown = false
    private var mScrollY = 0
    private var blockLoading = false

    private fun init() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }
    private fun observable(){
        viewModel.nowPlayingLD.observe(viewLifecycleOwner,{
            it?.let { response->
                if (viewModel.settingNowPlaying.lastPage!=1.0){
                    updateAdapter(response.results?: arrayListOf())
                }else{
                    setAdapter(FormatList.filterListByTime(response.results))
                }
            }
            viewModel.progressBarLD.value = false
            blockLoading = false
        })
        viewModel.sortingFieldLD.observe(viewLifecycleOwner,{
            if(viewModel.positionViewPager == numberViewPager) {
                setAdapter(FormatList.filterListByTime(viewModel.nowPlayingLD.value?.results, it))
            }
        })
    }

    private fun scrollListener(){
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                mScrollY += dy
                directionDown = dy > 0
            }
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (recyclerView.canScrollVertically(1).not()
                    && newState == RecyclerView.SCROLL_STATE_IDLE
                    && directionDown && !blockLoading && viewModel.canLoadingNowPlayingPage()) {
                    blockLoading = true
                    loadingNewItems()
                }
            }
        })
    }

    private fun loadingNewItems() {
        viewModel.loadingNewItems()
    }

    private fun updateAdapter(result: List<Result>) {
        val positionStart = if(result.size > viewModel.settingNowPlaying.lastItem) viewModel.settingNowPlaying.lastItem else 0.0
        adapter.list = result
        adapter.notifyItemRangeChanged(positionStart.toInt(), adapter.itemCount)
    }

    private fun setAdapter(result: List<Result>) {
        adapter = MoviesAdapter(result, callback)
        binding.recyclerView.layoutManager = GridLayoutManager(context,2, GridLayoutManager.VERTICAL, false)
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_movie_fragment,container,false)
        init()
        observable()
        initResultsSettings()
        showAdapter()
        setListeners()
        scrollListener()
        return binding.root
    }

    private fun initResultsSettings() {
        viewModel.settingNowPlaying = ListResultSetting(
            lastPage = 1.0,
            lastItem = viewModel.nowPlayingLD.value?.results?.lastIndex?.toDouble()?:0.0,
            totalPage = viewModel.nowPlayingLD.value?.totalPages?:0.0
        )
    }

    private fun setListeners() {
        binding.filterOption.setOnClickListener {
            activity?.supportFragmentManager?.let {
                FilterOptionDialogFragment.newInstance().show(it,"")
            }
        }
    }

    private fun showAdapter() {
        viewModel.nowPlayingLD.value?.let { response->
            setAdapter(response.results?: arrayListOf())
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(callback : ResultCallback, position : Int) = ListMovieFragment(callback, position)
    }
}
