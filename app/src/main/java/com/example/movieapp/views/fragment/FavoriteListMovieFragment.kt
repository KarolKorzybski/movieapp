package com.example.movieapp.views.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.movieapp.R
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.models.Result
import com.example.movieapp.utils.FormatList
import com.example.movieapp.viewmodels.MainViewModel
import com.example.movieapp.views.activity.MainActivity
import com.example.movieapp.views.adapter.MoviesAdapter
import com.example.movieapp.databinding.FavoriteListMovieFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteListMovieFragment(val callback : ResultCallback, private val numberViewPager : Int) : Fragment() {


    private lateinit var binding: FavoriteListMovieFragmentBinding
    private val viewModel: MainViewModel by activityViewModels()
    private lateinit var adapter: MoviesAdapter

    private fun init() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }
    private fun observable(){
        viewModel.sortingFieldLD.observe(viewLifecycleOwner,{
            if(viewModel.positionViewPager == numberViewPager) {
                setAdapter(FormatList.filterListByTime(viewModel.resultDao, it))
            }
        })
    }

    private fun getObservableDb() {
        viewModel.getResultDao().observe(viewLifecycleOwner, {
            viewModel.resultDao.clear()
            viewModel.resultDao.addAll(it?: arrayListOf())
            setAdapter(it?: arrayListOf())
        })
    }

    private fun setAdapter(result: List<Result>) {
        adapter = MoviesAdapter(result, callback)
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        staggeredGridLayoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        binding.recyclerView.layoutManager = staggeredGridLayoutManager
        binding.recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.favorite_list_movie_fragment,container,false)
        init()
        observable()
        getObservableDb()
        showAdapter()
        setListeners()
        return binding.root
    }

    private fun setListeners() {
        binding.filterOption.setOnClickListener {
            activity?.supportFragmentManager?.let {
                FilterOptionDialogFragment.newInstance().show(it,"")
            }
        }
    }

    private fun showAdapter() {
        setAdapter(viewModel.resultDao)
    }

    companion object {
        @JvmStatic
        fun newInstance(callback : ResultCallback, position : Int) = FavoriteListMovieFragment(callback, position)
    }
}
