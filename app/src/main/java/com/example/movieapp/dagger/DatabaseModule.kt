package com.example.movieapp.dagger

import android.content.Context
import androidx.room.Room
import com.example.movieapp.data.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.example.movieapp.BuildConfig
import com.example.movieapp.data.dao.ResultDao
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule() {


    @Provides
    @Singleton
    fun provideDb(@ApplicationContext context: Context): AppDatabase {
        return Room
            .databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME)
//                .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideResultDao(db: AppDatabase): ResultDao {
        return db.resultDao()
    }

}