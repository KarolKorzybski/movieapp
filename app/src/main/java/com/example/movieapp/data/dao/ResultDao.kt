package com.example.movieapp.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.movieapp.models.Result
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ResultDao {
    @Query("SELECT * FROM RESULT")
    fun getAll(): LiveData<List<Result>?>

    @Query("DELETE FROM RESULT")
    fun deleteAll() : Completable

    @Delete
    fun delete(entity: Result): Single<Int?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: List<Result>) : Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: Result) : Completable
}