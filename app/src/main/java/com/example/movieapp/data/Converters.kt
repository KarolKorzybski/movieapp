package com.example.movieapp.data

import androidx.room.TypeConverter
import com.example.movieapp.models.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.util.*
import java.util.Collections.emptyList
import kotlin.collections.ArrayList


class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    var gson = Gson()

    @TypeConverter
    fun stringToIntList(data: String?): List<Int?>? {
        if (data == null) {
            return emptyList()
        }
        val listType: Type = object : TypeToken<List<Int?>?>() {}.type
        return gson.fromJson<List<Int?>>(data, listType)
    }

    @TypeConverter
    fun intListToString(someObjects: List<Int?>?): String? {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun stringToStringList(data: String?): List<String?>? {
        if (data == null) {
            return emptyList()
        }
        val listType: Type = object : TypeToken<List<String?>?>() {}.getType()
        return gson.fromJson<List<String?>>(data, listType)
    }

    @TypeConverter
    fun stringListToString(someObjects: List<String?>?): String? {
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun fromStringToResultList(data: String?): ArrayList<Result>? {
        var mData = data
        if (mData == null) {
            mData = ""
        }
        val listType: Type = object : TypeToken<ArrayList<Result?>?>() {}.type
        return gson.fromJson(mData, listType)
    }

    @TypeConverter
    fun toStringFromResultList(someObjects: ArrayList<Result>?): String? {
        if (someObjects.isNullOrEmpty()) return ""
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun fromStringToDoubleList(data: String?): ArrayList<Double>? {
        var mData = data
        if (mData == null) {
            mData = ""
        }
        val listType: Type = object : TypeToken<ArrayList<Double?>?>() {}.type
        return gson.fromJson(mData, listType)
    }

    @TypeConverter
    fun toStringFromDoubleList(someObjects: ArrayList<Double>?): String? {
        if (someObjects.isNullOrEmpty()) return ""
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun fromStringToResult(data: String?): Result? {
        var mData = data
        if (mData == null) {
            mData = ""
        }
        val listType: Type = object : com.google.gson.reflect.TypeToken<Result?>() {}.type
        return gson.fromJson(mData, listType)
    }

    @TypeConverter
    fun toStringFromResult(someObjects: Result?): String? {
        if (someObjects == null) return ""
        return gson.toJson(someObjects)
    }

    @TypeConverter
    fun fromStringToDouble(data: String?): Double? {
        var mData = data
        if (mData == null) {
            mData = ""
        }
        val listType: Type = object : com.google.gson.reflect.TypeToken<Double?>() {}.type
        return gson.fromJson(mData, listType)
    }

    @TypeConverter
    fun toStringFromDouble(someObjects: Double?): String? {
        if (someObjects == null) return ""
        return gson.toJson(someObjects)
    }
}