package com.example.movieapp.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.movieapp.data.dao.ResultDao
import com.example.movieapp.models.Result

@Database(
        entities = [
            Result::class,
        ],
        version = 1,
        exportSchema = true
)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun resultDao(): ResultDao

}