package com.example.movieapp.api.response


import com.example.movieapp.models.Dates
import com.example.movieapp.models.Result
import com.google.gson.annotations.SerializedName

data class GetMoviesResultResponse(
    @SerializedName("dates")
    var dates: Dates?,
    @SerializedName("page")
    var page: Double?,
    @SerializedName("results")
    var results: List<Result>?,
    @SerializedName("total_pages")
    var totalPages: Double?,
    @SerializedName("total_results")
    var totalResults: Double?
)