package com.example.movieapp.api

import com.example.movieapp.api.response.GetMovieDetailsResponse
import com.example.movieapp.api.response.GetMoviesResultResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @Headers("Content-Type: application/json;charset=utf-8")
    @GET("/3/movie/now_playing/")
    fun getMovies(
        @Query("api_key") query: String?,
        @Query("language") language: String?,
        @Query("region") region: String?,
        @Query("page") page: Double?
    ): Observable<GetMoviesResultResponse?>

    @GET("/3/movie/{id}")
    fun getDetailsMovie(
        @Path("id") id: String?,
        @Query("api_key") key: String?,
        @Query("language") language: String?,
        @Query("region") region: String?
    ): Observable<GetMovieDetailsResponse?>

    @GET("/3/search/movie")
    fun getSearchMovies(
        @Query("query") query: String?,
        @Query("api_key") key: String?,
        @Query("language") language: String?,
        @Query("region") region: String?,
        @Query("page") page: Double
    ): Observable<GetMoviesResultResponse?>

}