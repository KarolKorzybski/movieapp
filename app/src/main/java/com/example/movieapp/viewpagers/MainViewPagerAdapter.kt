package com.example.movieapp.viewpagers

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.movieapp.callbacks.ResultCallback
import com.example.movieapp.views.fragment.FavoriteListMovieFragment
import com.example.movieapp.views.fragment.FindMoviesFragment
import com.example.movieapp.views.fragment.ListMovieFragment
import com.example.movieapp.views.fragment.MainMoviesFragment

class MainViewPagerAdapter(fm: FragmentManager, val callback : ResultCallback) : FragmentPagerAdapter(fm) {

    override fun getItem(i: Int): Fragment {
        return when (i) {
            0 -> ListMovieFragment(callback, i)
            1 -> FindMoviesFragment(callback, i)
            else -> FavoriteListMovieFragment(callback, i)
        }

    }

    override fun getCount(): Int {
        return 3
    }
}
