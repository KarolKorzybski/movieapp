package com.example.movieapp.repository

import com.example.movieapp.data.dao.ResultDao
import com.example.movieapp.models.Result
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.ArrayList
import javax.inject.Inject

class ResultDaoRepository @Inject constructor(
    private var resultDao: ResultDao
) {
    private fun setResults(field: List<Result>, isDone : () -> Unit) {
        resultDao
            .insert(field)
            .subscribeOn(Schedulers.newThread())
            .subscribe(object : DisposableCompletableObserver() {
                override fun onComplete() {
                    isDone()
                }

                override fun onError(e: Throwable) {
                    isDone()
                }

            })
    }

    private fun addResult(field: Result, isDone : () -> Unit) {
        resultDao
            .insert(field)
            .subscribeOn(Schedulers.newThread())
            .subscribe(object : DisposableCompletableObserver() {
                override fun onComplete() {
                    isDone()
                }

                override fun onError(e: Throwable) {
                    isDone()
                }

            })
    }

    private fun removeResult(field: Result, isDone : () -> Unit) {
        resultDao
            .delete(field)
            .subscribeOn(Schedulers.newThread())
            .subscribe(object : DisposableSingleObserver<Int?>() {

                override fun onError(e: Throwable) {
                    isDone()
                }

                override fun onSuccess(t: Int) {
                    isDone()
                }
            })
    }

    private fun setAndDeleteResults(field: List<Result>, isDone : () -> Unit) {
        resultDao
            .deleteAll()
            .subscribeOn(Schedulers.newThread())
            .subscribe(object : DisposableCompletableObserver() {
                override fun onComplete() {
                    setResults(field, isDone)
                }

                override fun onError(e: Throwable) {
                    setResults(field, isDone)
                }
            })
    }

    fun getAll() = resultDao.getAll()

    fun addResult(result: Result, resultDao: ArrayList<Result>, isDone : () -> Unit) {
        if(!isDuplicate(result, resultDao)){
            result.isFavorite = true
            addResult(result, isDone)
        }
    }

    fun removeResult(result: Result, resultDao: ArrayList<Result>, isDone : () -> Unit) {
        val list = resultDao
        list.indexOf(result).let {
            if (it != -1) {
                removeResult(result, isDone)
            }
        }
    }

    private fun isDuplicate(result: Result, resultDao: ArrayList<Result>) : Boolean{
        return !resultDao.filter { it.id == result.id }.isNullOrEmpty()
    }
}