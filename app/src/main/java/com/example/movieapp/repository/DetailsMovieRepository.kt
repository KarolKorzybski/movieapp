package com.example.movieapp.repository

import android.annotation.SuppressLint
import com.example.movieapp.BuildConfig
import com.example.movieapp.api.MovieApi
import com.example.movieapp.api.response.GetMovieDetailsResponse
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailsMovieRepository @Inject constructor(
    private var api: MovieApi
) {

    @SuppressLint("CheckResult")
    fun getMovieDetails(
        id: String,
        onSuccess: ((result: GetMovieDetailsResponse?) -> Unit),
        onError: ((error: Throwable?) -> Unit),
        onDone: ((Boolean) -> Unit)
    ) {
        api.getDetailsMovie(
            id,
            BuildConfig.API_KEY,
            "pl-PL",
            "PL"
        )
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    onSuccess.invoke(it)
                },
                { error ->
                    onError.invoke(error)
                },
                {
                    onDone.invoke(true)
                }
            )
    }
}