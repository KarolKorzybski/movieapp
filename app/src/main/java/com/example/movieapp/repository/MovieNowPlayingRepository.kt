package com.example.movieapp.repository

import android.annotation.SuppressLint
import com.example.movieapp.BuildConfig
import com.example.movieapp.api.MovieApi
import com.example.movieapp.api.response.GetMoviesResultResponse
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieNowPlayingRepository @Inject constructor(
    private var api: MovieApi
) {

    @SuppressLint("CheckResult")
    fun getNowPlaying(
        page : Double = 1.0,
        onSuccess: ((result: GetMoviesResultResponse?) -> Unit),
        onError: ((error: Throwable?) -> Unit),
        onDone: ((Boolean) -> Unit)
    ) {
        api.getMovies(
            BuildConfig.API_KEY,
            "pl-PL",
            "PL",
            page
        )
            .subscribeOn(Schedulers.newThread())
            .subscribe(
                {
                    onSuccess.invoke(it)
                },
                { error ->
                    onError.invoke(error)
                },
                {
                    onDone.invoke(true)
                }
            )
    }
}