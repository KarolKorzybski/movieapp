package com.example.movieapp.models

import com.google.gson.annotations.SerializedName


data class Genre(
    @SerializedName("id")
    var id: Double?,
    @SerializedName("name")
    var name: String?
)