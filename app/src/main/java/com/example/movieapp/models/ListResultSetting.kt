package com.example.movieapp.models

data class ListResultSetting(
    var lastPage : Double = 1.0,
    var lastItem : Double = 0.0,
    var totalPage : Double = 0.0
)
