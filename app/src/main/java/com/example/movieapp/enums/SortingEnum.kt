package com.example.movieapp.enums

enum class SortingEnum {
    POPULARITY, LIKE, DATE
}