package com.example.movieapp.layouts

import android.content.Context
import android.text.*
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.example.movieapp.R
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.alpha
import com.mikhaellopez.rxanimation.scale


class AnimationChangeLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defstyl: Int = 0
) : FrameLayout(context, attrs, defstyl) {

    private lateinit var actionChange: FrameLayout
    private var defaultShow: Boolean = false
    var callbackDoneAction : ((Boolean) -> Unit)? = null


    fun initField() {
    }

    private fun init() {
        inflate(context, itemLayout, this)
        actionChange = findViewById(R.id.actionChange)
    }

    init {
        init()
        initField()
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.AnimationChangeLayout,
            0, 0
        ).apply {
            try {
                defaultShow = getBoolean(R.styleable.AnimationChangeLayout_default_show, false)
                showHideView(defaultShow)
            } finally {
                recycle()
            }
            invalidate()
            requestLayout()
        }

    }

    private fun showHideView(isShow: Boolean) {
        if(isShow){
            actionChange.alpha = 1.0f
            actionChange.visibility = View.VISIBLE
        }else{
            actionChange.alpha = 0.0f
            actionChange.visibility = View.GONE
        }
    }

    fun initAnimation(show : Boolean, callbackDoneAction : ((Boolean) -> Unit)) {
        actionChange.alpha = if(show) 0.0f else 1.0f
        actionChange.visibility = View.VISIBLE
        actionChange.scale(if(show) 0.0f else 1.0f,duration = 0).subscribe()
        RxAnimation.together(
            actionChange.scale(if(show) 1.0f else 0.0f,duration = 500),
            actionChange.alpha(if(show) 1.0f else 0.0f,duration = 500)
        ).subscribe {
            callbackDoneAction.invoke(true)
            if(!show) actionChange.visibility = View.GONE
        }
    }

    val itemLayout: Int
        get() = R.layout.animation_change_layout

}