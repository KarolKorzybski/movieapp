package com.example.movieapp.layouts

import android.content.Context
import android.text.*
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.example.movieapp.R
import com.mikhaellopez.rxanimation.RxAnimation
import com.mikhaellopez.rxanimation.alpha
import com.mikhaellopez.rxanimation.scale


class ProgressLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defstyl: Int = 0
) : FrameLayout(context, attrs, defstyl) {

    private lateinit var logo: ImageView

    fun initField() {
    }

    private fun init() {
        inflate(context, itemLayout, this)
        logo = findViewById(R.id.logo)
    }

    init {
        init()
        initField()
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.ProgressLayout,
            0, 0
        ).apply {
            try {
            } finally {
                recycle()
            }
            invalidate()
            requestLayout()
        }
        initAnimation()

    }

    private fun initAnimation() {
        RxAnimation.from(logo).alpha(alpha = 0.25f, duration = 1000, reverse = true).repeat()
            .subscribe()
        RxAnimation.from(logo).scale(scale = 0.75f, duration = 1000, reverse = true).repeat()
            .subscribe()
    }

    val itemLayout: Int
        get() = R.layout.progress_layout

}